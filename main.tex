\documentclass[a4paper, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=0.3cm]{caption}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{xspace}
\usepackage{hyperref}

\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    }

\title{Expected Performance of the ATLAS $b$-jet Trigger Algorithms for the Start of LHC Run 3}
\author{Maggie Chen, Dan Guest, Chris Pollard, Victor Hugo Ruelas Rivera}
\date{June 2022}

\newcommand{\citeme}{{\color{red} CITE}\xspace}

\begin{document}

\maketitle

\begin{abstract}
    \noindent We present the expected performance of the ATLAS HLT $b$-tagging
    algorithms to be used at the start of Run 3 LHC data taking.
    The introduction of the DL1 family of $b$-tagging algorithms into the ATLAS
    trigger has yielded much stronger expected discrimination of light- and
    heavy-flavor jets, resulting in higher trigger efficiencies of interesting
    physics signatures at an affordable readout rate.
    These algorithmic improvements are critical for trigger signatures involving
    $b$-jet final states used in a variety of ATLAS analyses, including the
    search for di-Higgs production.
\end{abstract}

\newcommand{\explaind}{%
$D = \log ( p_b / [ p_c f_c + p_u  (1-f_c) ] )$,
where $p_b$, $p_c$ and $p_u$ are the $b$-, $c$-, and light-jet probability outputs of the DL1d algorithm,
and $f_c$ is the effective $c$-jet fraction\xspace
}

\newcommand{\explaindbb}{%
    $D = \log ( p_b / p_{bb} )$,
    where $p_b$ and $p_{bb}$ are the $b$-jet and $bb$-jet outputs for the DL1dbb algorithm\xspace}

\newcommand{\explaineff}[1]{%
    Jets are labeled as $b$, $c$, or light using simulation-based hadron matching as described in previous ATLAS results [\href{https://arxiv.org/abs/1907.05120}{#1}]\xspace
}
\newcommand{\explaineffbb}[1]{%
    Jets are labeled as $bb$, $b$, $c$, or light using simulation-based hadron matching as described in previous ATLAS results [\href{https://arxiv.org/abs/1907.05120}{#1}], where $bb$ requires two matched $b$-hadrons\xspace
}


\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{DL1d_disc.pdf}
    \caption{
        Distributions of the $b$-jet discriminant score for the DL1d algorithm \explaind.
        The DL1d algorithm uses the DL1
        architecture
            [\href{https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/FTAG-2018-01/}{1}]
        and the DIPS low-level tagger
            [\href{https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2020-014/}{2}]. Both were trained with simulated $t\bar{t}$ and $Z'$ events.
        The algorithms are evaluated on HLT Particle Flow jets from a $t\bar{t}$ sample.
        The $b$-jet (blue), $c$-jet (orange) and light-flavor jet (green)
        distributions are shown for $f_c = 0.018$.
        Red vertical lines indicate the 85\%, 77\%, 70\% and 60\% $b$-jet efficiency
        working points.
        \explaineff{3}.
    }
    %% }
    \label{fig:DL1d_disc}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{DL1d_ujet_ROC.pdf}
    \caption{
        Light-flavor jet rejection as a function of $b$-jet efficiency of the
        DIPS (green dashed) and DL1d (blue dash-dotted) algorithms in comparison to the benchmark DL1r
        algorithm
            [\href{https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/FTAG-2019-005/}{1}]
        (brown solid), evaluated on HLT Particle Flow
        jets in a $t\bar{t}$ sample. \explaineff{2}.
        The 60\%, 70\%, 77\% and 85\% $b$-jet efficiency working points are
        indicated by vertical red lines.
        In all cases the discriminant is given by \explaind.
    }
    \label{fig:DL1d_ujet_roc}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{DL1d_cjet_ROC.pdf}
    \caption{
        $c$-jet rejection as a function of $b$-jet efficiency of the
        DIPS (green dashed) and DL1d (blue dash-dotted) algorithms in comparison to
        the benchmark DL1r algorithm (brown solid), evaluated on HLT Particle Flow
        jets in a $t\bar{t}$ sample. \explaineff{1}.
        The 60\%, 70\%, 77\% and 85\% $b$-jet efficiency working points are
        indicated by vertical red lines.
        In all cases the discriminant is given by \explaind.
    }
    \label{fig:DL1d_cjet_roc}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.6]{scores_DL1d20211216_bb_ICHEP_299.pdf}
    \caption{
        Distributions of the DL1d discriminant evaluated on Particle Flow HLT jets
        from $t\bar{t}$ and multijet samples.
        The 77\% $b$-jet efficiency working point is indicated by a vertical red
        line.
        The discriminant distributions are shown separately for jets of various
        flavors for $f_c = 0.018$, with significant overlap observed in the discriminant
        distributions for jets containing exactly one $b$-hadron ($b$-jets) and those
        containing more than one ($bb$-jets).
        The discriminant is given by \explaind. \explaineffbb{1}.
    }
    \label{fig:DL1d_withbb}
\end{figure}


\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.6]{scores_DL1dbb_ICHEP_299.pdf}
    \caption{
        Distributions of discriminant scores for the DL1dbb algorithm, trained on
        a mixture of $t\bar{t}$ and multijet events.
        The discriminant is defined by \explaindbb which
        uses the DL1 architecture and the DIPS low-level tagger to discriminant
        between $b$ and $bb$-jets. The tagger is evaluated on Particle Flow HLT jets from $t\bar{t}$
        and multijet samples. The 77\% $b$-jet efficiency working point is indicated by
        a vertical red line.
        \explaineffbb{1}.
    }
    \label{fig:DL1d_bb_disc}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.6]{DL1d_bb_flavour_DL1d20211216_ICHEP_299.pdf}
    \caption{
        $bb$-jet rejection as a function of $b$-jet efficiency of the
        DL1d (blue) and DL1dbb (orange) algorithms, evaluated on HLT Particle Flow
        jets from $t\bar{t}$ and multijet samples. \explaineffbb{1}.
        The discriminant is defined by \explaindbb.
        The DL1dbb performance is conditional on a DL1d discriminant cut at the 85\%
        working point.
        The 60\%, 70\%, 77\% and 85\% $b$-jet efficiency working points are
        indicated by vertical red lines.
    }
    \label{fig:DL1d_bb_rej}
\end{figure}

\newcommand{\explainrate}[1]{%
    Rates are estimated with Run 2 Enhanced Bias Data
        [\href{https://cds.cern.ch/record/2223498/files/ATL-DAQ-PUB-2016-002.pdf}{#1}],
    and the $b$-jet efficiencies are estimated for $t\bar{t}$ and multijet samples using
    trigger Particle Flow jets\xspace
}


\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.6]{Trigger_rates_16_05_DL1d.pdf}
    \caption{
        Expected trigger rates as a function of $b$-tagging efficiency using the
        DL1d algorithm when requiring at least four Particle Flow jets, three of
        which are requirement to be above the $b$-tagging threshold. \explaineff{1}.
        \explainrate{2}. The relative errors on trigger rates are between 10\% and 21\%.
    }
    \label{fig:DL1d_trig_rate}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.6]{Trigger_rates_16_05_DL1dbb.pdf}
    \caption{
        Expected trigger rates as a function of $b$-tagging efficiency
        while requiring both a variable DL1dbb threshold and a fixed DL1d
        threshold at the 85\% working point. \explaineffbb{1}.
        Here at least four Particle Flow jets are required, three of which should be
        above the $b$-tagging threshold.
        \explainrate{2}. The relative errors on the trigger rates are between 10\% and 17\%.
    }
    \label{fig:DL1dbb_trig_rate}
\end{figure}

\end{document}
